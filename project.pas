program project;
Uses crt;
Const count=17;
      min=-10;
Type Vector=array[1..count] of integer;
     Matrix=array[1..6, 1..6] of integer;
Var DataArray:Vector;
    DataMatrix:Matrix;
Procedure CreateArray (var _DataArray:Vector);
        var i:byte;
        begin
            For i:=1 to count do
            _DataArray[i]:=Random(22)-9;
        end;
Procedure PrintArray (var _DataArray:Vector);
        var i:byte;
        begin
            For i:=1 to count do
            Write(_DataArray[i]:3);
            Writeln;
        end;
Procedure FindTranslocationZero(var _DataArray:Vector); {Задача1}
          var i,j:byte;
              begin
                  For i:=1 to count do
                  begin
                       If _DataArray[i]=0 then
                       begin
                            For j:=i-1 downto 1 do
                            _DataArray[j+1]:=_DataArray[j];
                            _DataArray[1]:=0;
                       end;

                  end;
              end;
Function FindMax(var _DataArray:Vector):integer;
          var i,_max:integer;
              begin
                  _max:=min;
                  For i:=1 to count do
                  If _DataArray[i]>_max then _max:=_DataArray[i];
                  FindMax:=_max;
              end;
Function FindNumberMax(var _DataArray:Vector):integer;
          var i,_max:integer;
              begin
                  _max:=min;
                  For i:=1 to count do
                  If _DataArray[i]>_max then
                     begin
                          _max:=_DataArray[i];
                          FindNumberMax:=i;
                     end;
              end;
Procedure TranslocationMax ( var _DataArray:Vector);
          var i,j,max:integer;
          begin
               max:=FindMax(DataArray);
               For j:=FindNumberMax(DataArray) to count-1 do
               if _DataArray[j]=max then
                   begin
                        _DataArray[j]:=_DataArray[j+1];
                        _DataArray[count]:=max;
                   end;
          end;

Procedure CreateMatrix (Var _DataMatrix:Matrix);
          var i,j:integer;
          begin
               For i:=1 to 6 do
                for j:=1 to 6 do
                 _DataMatrix[i,j]:=random(38)-17;
          end;
Procedure PrintMatrix(_DataMatrix:Matrix);
          var i,j:integer;
          begin
               For i:=1 to 6 do
                   begin
                      for j:=1 to 6 do
                         Write(_DataMatrix[i,j]:4);
                      writeln;
                   end;
                   end;


Procedure FindMaxMainDiagonal(_DataMatrix:Matrix);
          var i,max, max_i:integer;
            begin
                 max:=_DataMatrix[1,1];
                 max_i:=1;
                 for i:=1 to 6 do
                     if _DataMatrix[i,i]>max then
                         begin
                              max:=_DataMatrix[i,i];
                              max_i:=max;
                         end;
                 writeln('Max= ', max, 'I= ', max_i);

            end;

Begin
  Randomize;
  CreateArray(DataArray);
  Writeln('Array');
  PrintArray(DataArray);
  FindTranslocationZero(DataArray);
  Writeln('Array after moving zeros');
  PrintArray(DataArray);
  TranslocationMax(DataArray);
   Writeln('Array after moving max');
  PrintArray(DataArray);

  CreateMatrix(DataMatrix);
  Writeln('Matrix');
  PrintMatrix(DataMatrix);

  Writeln;
  FindMaxMainDiagonal(DataMatrix);

  Repeat until keypressed;
End.

